import serial
from datetime import datetime
from time import sleep

ser = serial.Serial('/dev/ttyACM0',9600)
#s = [0,1]


print("Starting Sensor Read Loop", flush=True)

while True:
    read_serial = ser.readline()
    read_serial = read_serial.decode()
    serial_str = read_serial.rstrip("\n \r") 
    now_str = datetime.now().strftime("%Y-%m-%d %H:%M")
    print(f"{now_str} ** {serial_str}", flush=True)

